create sequence employee_seq;

create table employee
(
    id                 bigint      not null
        primary key,
    created_by         varchar(50) not null,
    created_date       timestamp(6),
    last_modified_by   varchar(50),
    last_modified_date timestamp(6),
    email              varchar(255),
    num_of_project     integer,
    password           varchar(255),
    role               varchar(255),
    username           varchar(255),
    fullname           varchar(255),
    uuid               varchar(255)
        constraint uks92jep3t60waoctlvxre09vf
            unique
);




