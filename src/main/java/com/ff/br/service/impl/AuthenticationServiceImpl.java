package com.ff.br.service.impl;

import com.ff.br.domain.dto.request.SignInRequest;
import com.ff.br.domain.dto.response.SignInResponse;
import com.ff.br.domain.entity.Employee;
import com.ff.br.repository.EmployeeRepository;
import com.ff.br.service.AuthenticationService;
import com.ff.br.service.JwtService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

/** AuthenticationServiceImpl */
@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

  private final EmployeeRepository employeeRepository;
  private final JwtService jwtService;
  private final AuthenticationManager authenticationManager;

  @Override
  public SignInResponse signIn(SignInRequest request) {
    var userOptional = employeeRepository.findByUsername(request.username());
    if (userOptional.isPresent()) {
      Employee user = userOptional.get();
      authenticationManager.authenticate(
          new UsernamePasswordAuthenticationToken(request.username(), request.password()));
      var tokenMap = jwtService.generateToken(user);
      return SignInResponse.builder()
          .accessToken(String.valueOf(tokenMap.get("accessToken")))
          .refreshToken(String.valueOf(tokenMap.get("refreshToken")))
          .type("Bearer")
          .expiredIn((Long) tokenMap.get("expiredIn"))
          .username(user.getUsername())
          .build();
    } else {
      return null;
    }
  }
}
