package com.ff.br.service.impl;

import com.ff.br.domain.dto.request.MailRequest;
import com.ff.br.domain.enums.EmailType;
import com.ff.br.service.MailService;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

/** MailServiceImpl */
@Slf4j
@Service
@RequiredArgsConstructor
public class MailServiceImpl implements MailService {

  @Value("${spring.mail.username}")
  private String sender;

  private final TemplateEngine templateEngine;
  private final JavaMailSender javaMailSender;

  @Override
  public String buildTemplate(MailRequest mailRequest) {
    Context context = new Context();
    context.setVariable("receiver", mailRequest.receiver());
    context.setVariable("sender", sender);
    Map<String, Object> mailProperties = mailRequest.mailProperties();
    if (EmailType.PAYROLL.equals(mailRequest.emailType())) {
      context.setVariable("lastDayOfMonth", mailProperties.get("lastDayOfMonth"));
      context.setVariable("deadLineMonth", mailProperties.get("deadLineMonth"));
      context.setVariable("adminEmail", sender);
      return templateEngine.process(mailRequest.emailType().getTemplateName(), context);
    }
    return null;
  }

  @Override
  @Async("threadPoolTaskExecutor")
  public CompletableFuture<Boolean> sendAttachment(MailRequest mailRequest) {
    MimeMessage message = javaMailSender.createMimeMessage();
    FileInputStream inputStream = null;
    try {
      Map<String, Object> mailProperties = mailRequest.mailProperties();
      MimeMessageHelper helper = new MimeMessageHelper(message, true);
      helper.setFrom(sender);
      helper.setTo(mailRequest.receiver());
      helper.setSubject(String.valueOf(mailProperties.get("subject")));
      helper.setText(String.valueOf(mailProperties.get("bodyHTML")), true);
      inputStream = mailRequest.attachment();
      helper.addAttachment(
          String.valueOf(mailProperties.get("fileName")),
          new ByteArrayResource(inputStream.readAllBytes()));
      javaMailSender.send(message);
      return CompletableFuture.completedFuture(Boolean.TRUE);
    } catch (MessagingException | IOException e) {
      return CompletableFuture.completedFuture(Boolean.FALSE);
    } finally {
      try {
        assert inputStream != null;
        inputStream.close();
      } catch (IOException | NullPointerException e) {
        e.printStackTrace();
      }
    }
  }
}
