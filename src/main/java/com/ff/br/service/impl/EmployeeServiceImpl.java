package com.ff.br.service.impl;

import com.ff.br.domain.dto.request.MailRequest;
import com.ff.br.domain.entity.Employee;
import com.ff.br.domain.enums.EmailType;
import com.ff.br.helper.DataHelper;
import com.ff.br.repository.EmployeeRepository;
import com.ff.br.service.EmployeeService;
import com.ff.br.service.MailService;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.LocalDate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/** EmployeeServiceImpl */
@Slf4j
@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

  private final MailService mailService;

  private final EmployeeRepository employeeRepository;

  @Override
  public Map<String, Boolean> sendPayrollMonthly() {
    Map<String, Boolean> result = new HashMap<>();
    List<Employee> employees = employeeRepository.findAll();
    for (Employee employee : employees) {
      String email = employee.getEmail();
      String lastDayOfMonth = DataHelper.getLastDayOfMonthUsingJodaTime(LocalDate.now());
      MailRequest mailRequest = buildMailRequest(employee, lastDayOfMonth);
      try {
        Boolean isCompleted = mailService.sendAttachment(mailRequest).get();
        result.put(email, isCompleted);
      } catch (Exception e) {
        result.put(email, Boolean.FALSE);
      }
    }
    return result;
  }

  private MailRequest buildMailRequest(Employee employee, String lastDayOfMonth) {
    Map<String, Object> mailProperties = new HashMap<>();
    mailProperties.put(
        "lastDayOfMonth",
        java.time.LocalDate.now().getMonth().name() + " " + java.time.LocalDate.now().getYear());
    mailProperties.put("deadLineMonth", lastDayOfMonth);
    MailRequest mailRequest =
        new MailRequest(
            employee.getEmail(), mailProperties, retrievePayMonthFile(employee), EmailType.PAYROLL);
    mailProperties.put("fileName", "payroll.xlsx");
    mailProperties.put("subject", EmailType.PAYROLL.getSubject());
    String template = mailService.buildTemplate(mailRequest);
    mailProperties.put("bodyHTML", template);
    return mailRequest;
  }

  private FileInputStream retrievePayMonthFile(Employee employee) {
    try {
      return new FileInputStream(
          "D:\\code\\2ff\\br\\src\\main\\resources\\templates\\payroll.xlsx");
    } catch (FileNotFoundException e) {
      log.error("FileNotFoundException is thrown: {}", e.getMessage());
      return null;
    }
  }

  @Override
  public Page<Employee> getEmployee(Pageable pageable) {
    return employeeRepository.findAll(pageable);
  }

  @Override
  public void deleteEmployee(List<String> uuid) {}
}
