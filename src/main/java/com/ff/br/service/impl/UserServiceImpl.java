package com.ff.br.service.impl;

import com.ff.br.repository.EmployeeRepository;
import com.ff.br.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/** UserServiceImpl */
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
  private final EmployeeRepository employeeRepository;

  @Override
  public UserDetailsService userDetailsService() {
    return username ->
        employeeRepository
            .findByUsername(username)
            .orElseThrow(() -> new UsernameNotFoundException("User not found"));
  }
}
