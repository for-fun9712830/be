package com.ff.br.service.impl;

import com.ff.br.service.JwtService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

/** JwtServiceImpl */
@Slf4j
@Service
public class JwtServiceImpl implements JwtService {

  @Value("${token.signing.access_token}")
  private String accessTokenKey;

  @Value("${token.signing.refresh_token}")
  private String refreshTokenKey;

  @Override
  public String extractUserName(String token) {
    return extractClaim(token, Claims::getSubject);
  }

  @Override
  public Map<String, Object> generateToken(UserDetails userDetails) {
    return buildClaims(new HashMap<>(), userDetails);
  }

  @Override
  public boolean isTokenValid(String token, UserDetails userDetails) {
    final var userName = extractUserName(token);
    return (userName.equals(userDetails.getUsername())) && !isTokenExpired(token);
  }

  private <T> T extractClaim(String token, Function<Claims, T> claimsResolvers) {
    final var claims = extractAllClaims(token);
    return claimsResolvers.apply(claims);
  }

  private Map<String, Object> buildClaims(
      Map<String, Object> extraClaims, UserDetails userDetails) {
    var objectMap = new HashMap<String, Object>();
    var expiredIn = System.currentTimeMillis() + 1000 * 60;
    var accessToken =
        Jwts.builder()
            .setClaims(extraClaims)
            .setSubject(userDetails.getUsername())
            .setIssuedAt(new Date(System.currentTimeMillis()))
            .setExpiration(new Date(expiredIn * 24))
            .signWith(getSigningKey(accessTokenKey), SignatureAlgorithm.HS256)
            .compact();
    var refreshToken =
        Jwts.builder()
            .setClaims(extraClaims)
            .setSubject(userDetails.getUsername())
            .setIssuedAt(new Date(System.currentTimeMillis()))
            .setExpiration(new Date(expiredIn * 24))
            .signWith(getSigningKey(refreshTokenKey), SignatureAlgorithm.HS256)
            .compact();
    objectMap.put("expiredIn", expiredIn);
    objectMap.put("accessToken", accessToken);
    objectMap.put("refreshToken", refreshToken);
    return objectMap;
  }

  private boolean isTokenExpired(String token) {
    return extractExpiration(token).before(new Date());
  }

  private Date extractExpiration(String token) {
    return extractClaim(token, Claims::getExpiration);
  }

  private Claims extractAllClaims(String token) {
    return Jwts.parserBuilder()
        .setSigningKey(getSigningKey(accessTokenKey))
        .build()
        .parseClaimsJws(token)
        .getBody();
  }

  private Key getSigningKey(String key) {
    byte[] keyBytes = Decoders.BASE64.decode(key);
    return Keys.hmacShaKeyFor(keyBytes);
  }
}
