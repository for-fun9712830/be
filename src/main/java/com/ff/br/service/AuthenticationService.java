package com.ff.br.service;

import com.ff.br.domain.dto.request.SignInRequest;
import com.ff.br.domain.dto.response.SignInResponse;

/** AuthenticationService */
public interface AuthenticationService {
  SignInResponse signIn(SignInRequest request);
}
