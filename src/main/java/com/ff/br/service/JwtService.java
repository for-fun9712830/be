package com.ff.br.service;

import java.util.Map;
import org.springframework.security.core.userdetails.UserDetails;

/** JwtService */
public interface JwtService {
  String extractUserName(String token);

  Map<String, Object> generateToken(UserDetails userDetails);

  boolean isTokenValid(String token, UserDetails userDetails);
}
