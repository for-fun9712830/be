package com.ff.br.service;

import com.ff.br.domain.dto.request.MailRequest;
import java.util.concurrent.CompletableFuture;

/** MailService */
public interface MailService {

  String buildTemplate(MailRequest mailRequest);

  CompletableFuture<Boolean> sendAttachment(MailRequest mailRequest);
}
