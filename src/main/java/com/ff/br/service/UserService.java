package com.ff.br.service;

import org.springframework.security.core.userdetails.UserDetailsService;

/** UserService */
public interface UserService {

  UserDetailsService userDetailsService();
}
