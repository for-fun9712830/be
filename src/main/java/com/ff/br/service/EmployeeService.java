package com.ff.br.service;

import com.ff.br.domain.entity.Employee;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/** EmployeeService */
public interface EmployeeService {
  Map<String, Boolean> sendPayrollMonthly();

  Page<Employee> getEmployee(Pageable pageable);

  void deleteEmployee(List<String> uuid);
}
