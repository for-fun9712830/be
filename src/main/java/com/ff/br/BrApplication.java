package com.ff.br;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/** BrApplication */
@SpringBootApplication
public class BrApplication {

  public static void main(String[] args) {
    SpringApplication.run(BrApplication.class, args);
  }
}
