package com.ff.br.scheduler;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

/** BotReminder */
@Component
@EnableScheduling
@RequiredArgsConstructor
public class BotReminder {}
