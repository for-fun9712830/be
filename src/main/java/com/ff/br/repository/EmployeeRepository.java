package com.ff.br.repository;

import com.ff.br.domain.entity.Employee;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

/** EmployeeRepository */
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
  Optional<Employee> findByUsername(String username);

  Optional<Employee> findByUuid(String uuid);
}
