package com.ff.br.resource.api;

import com.ff.br.domain.dto.response.ApiResponse;
import com.ff.br.domain.entity.Employee;
import com.ff.br.service.EmployeeService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/** EmployeeController */
@RequestMapping("/api/v1/employee")
@RestController
@RequiredArgsConstructor
public class EmployeeController {
  private final EmployeeService employeeService;

  @GetMapping("/payroll")
  public ResponseEntity<ApiResponse<?>> sendPayrollMonthly() {
    return ResponseEntity.ok(new ApiResponse<>().success(employeeService.sendPayrollMonthly()));
  }

  @GetMapping
  public ResponseEntity<Page<Employee>> getAllEmployee(Pageable pageable) {
    Page<Employee> employeeResponses = employeeService.getEmployee(pageable);
    return ResponseEntity.ok(employeeResponses);
  }

  @DeleteMapping
  public ResponseEntity<Void> deleteEmployee(@RequestParam(name = "uuid") List<String> uuid) {
    employeeService.deleteEmployee(uuid);
    return ResponseEntity.ok().build();
  }
}
