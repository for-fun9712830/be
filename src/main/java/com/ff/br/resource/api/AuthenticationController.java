package com.ff.br.resource.api;

import com.ff.br.domain.dto.request.SignInRequest;
import com.ff.br.domain.dto.response.ApiResponse;
import com.ff.br.domain.dto.response.SignInResponse;
import com.ff.br.service.AuthenticationService;
import java.util.Locale;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/** AuthenticationController */
@RequestMapping("/api/v1/authenticate")
@RestController
@RequiredArgsConstructor
public class AuthenticationController {

  private final AuthenticationService authenticationService;
  private final MessageSource messageSource;

  @PostMapping
  public ResponseEntity<ApiResponse<?>> signIn(@RequestBody SignInRequest request) {
    SignInResponse signInResponse = authenticationService.signIn(request);

    if (signInResponse != null) {
      return ResponseEntity.ok(new ApiResponse<>().success(signInResponse));
    } else {
      return ResponseEntity.badRequest()
          .body(
              new ApiResponse<>()
                  .badRequest(
                      messageSource.getMessage(
                          "invalid.authenticate.message", null, Locale.ENGLISH)));
    }
  }
}
