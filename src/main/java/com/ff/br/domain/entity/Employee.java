package com.ff.br.domain.entity;

import com.ff.br.domain.converter.RoleConverter;
import com.ff.br.domain.enums.Role;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.ConverterRegistration;
import org.hibernate.annotations.UuidGenerator;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/** Employee */
@Getter
@ToString
@Entity
@Table(indexes = @Index(columnList = "uuid", unique = true))
@Builder
@AllArgsConstructor
@Setter
@NoArgsConstructor
@ConverterRegistration(converter = RoleConverter.class)
public class Employee extends AbstractEntity implements Serializable, UserDetails {
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "employee_seq")
  @SequenceGenerator(sequenceName = "employee_seq", name = "employee_seq", allocationSize = 1)
  @Id
  @Column(name = "id")
  private Long id;

  @UuidGenerator
  @Column(name = "uuid")
  private String uuid;

  @Column(name = "username")
  private String username;

  @Column(name = "password")
  private String password;

  @Column(name = "role")
  private Role role;

  @Column(name = "email")
  private String email;

  @Column(name = "num_of_project")
  private Integer numOfProject;

  @Column(name = "fullname")
  private String fullName;

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return List.of(new SimpleGrantedAuthority(role.name()));
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return true;
  }
}
