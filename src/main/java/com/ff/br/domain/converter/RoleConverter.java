package com.ff.br.domain.converter;

import com.ff.br.domain.enums.Role;
import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import java.util.Objects;

/** RoleConverter */
@Converter
public class RoleConverter implements AttributeConverter<Role, String> {
  @Override
  public String convertToDatabaseColumn(Role role) {
    if (Objects.isNull(role)) {
      return null;
    }
    return role.getValue();
  }

  @Override
  public Role convertToEntityAttribute(String dbData) {
    return Role.of(dbData);
  }
}
