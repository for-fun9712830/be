package com.ff.br.domain.enums;

import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Getter;

/** EmailType */
@Getter
@AllArgsConstructor
public enum EmailType {
  PAYROLL("PAYROLL MONTHLY", "payroll");

  private final String subject;
  private final String templateName;

  private static final Map<String, EmailType> EmailTypeMap = new HashMap<>();

  static {
    for (EmailType type : EmailType.values()) {
      EmailTypeMap.put(type.templateName, type);
    }
  }

  public static EmailType of(String s) {
    return EmailTypeMap.get(s);
  }
}
