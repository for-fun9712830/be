package com.ff.br.domain.enums;

import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Getter;

/** Role */
@Getter
@AllArgsConstructor
public enum Role {
  ROLE_ADMIN(1, "ROLE_ADMIN"),
  ROLE_USER(0, "ROLE_USER");

  private final Integer key;
  private final String value;

  private static final Map<String, Role> roleMap = new HashMap<>();

  static {
    for (Role role : Role.values()) {
      roleMap.put(role.value, role);
    }
  }

  public static Role of(String s) {
    return roleMap.get(s);
  }
}
