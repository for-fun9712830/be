package com.ff.br.domain.enums;

import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Getter;

/** KokKatBotConversation */
@Getter
@AllArgsConstructor
public enum KokKatBotConversation {
  HI("@kokkak_bot", "Hi, Can I help you ?"),
  ON_LEAVE("@kokkak_bot on_leave", "Noted, take care broooo"),
  NO_RESPONSE("", "Sorry, I don't have any answer"),
  REMINDER_CHECK_IN("", "@minhnguyen239 Remember to check-in for today, thanks a lot"),
  REMINDER_REPORT("", "@minhnguyen239 Remember to report for today, thanks a lot");

  private final String request;
  private final String response;

  private static final Map<String, KokKatBotConversation> TelegramBotCmdMap = new HashMap<>();

  static {
    for (KokKatBotConversation botConversation : KokKatBotConversation.values()) {
      TelegramBotCmdMap.put(botConversation.getRequest(), botConversation);
    }
  }

  public static KokKatBotConversation of(String s) {
    return TelegramBotCmdMap.get(s);
  }
}
