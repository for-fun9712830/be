package com.ff.br.domain.enums;

import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Getter;

/** FileType */
@Getter
@AllArgsConstructor
public enum FileType {
  PDF("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"),
  HTML("text/html; charset=UTF-8");

  private final String extension;

  private static final Map<String, FileType> fileTypeMap = new HashMap<>();

  static {
    for (FileType type : FileType.values()) {
      fileTypeMap.put(type.getExtension(), type);
    }
  }

  public static FileType of(String s) {
    return fileTypeMap.get(s);
  }
}
