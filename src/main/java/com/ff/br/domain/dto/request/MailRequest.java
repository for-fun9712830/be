package com.ff.br.domain.dto.request;

import com.ff.br.domain.enums.EmailType;
import java.io.FileInputStream;
import java.util.Map;

/**
 * MailRequest
 *
 * @param receiver
 * @param mailProperties
 * @param attachment
 * @param emailType
 */
public record MailRequest(
    String receiver,
    Map<String, Object> mailProperties,
    FileInputStream attachment,
    EmailType emailType) {}
