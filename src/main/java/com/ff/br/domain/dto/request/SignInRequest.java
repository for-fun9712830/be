package com.ff.br.domain.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * SignInRequest
 *
 * @param username
 * @param password
 */
public record SignInRequest(
    @JsonProperty("username") String username, @JsonProperty("password") String password) {}
