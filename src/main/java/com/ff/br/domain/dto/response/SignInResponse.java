package com.ff.br.domain.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;

/** SignInResponse */
@Builder
@Getter
public class SignInResponse {
  @JsonProperty("access_token")
  private String accessToken;

  @JsonProperty("refresh_token")
  private String refreshToken;

  @JsonProperty("type")
  private String type;

  @JsonProperty("expired_in")
  private Long expiredIn;

  @JsonProperty("username")
  private String username;
}
