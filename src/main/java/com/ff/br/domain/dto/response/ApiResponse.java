package com.ff.br.domain.dto.response;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

/**
 * ApiResponse
 *
 * @param <T>
 */
@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ApiResponse<T> {

  private int errorCode;
  private T data;
  private String message;
  private LocalDateTime timestamp;

  public ApiResponse<T> success(T data) {
    return new ApiResponse<>(
        HttpStatus.OK.value(), data, HttpStatus.OK.name(), LocalDateTime.now());
  }

  public ApiResponse<T> badRequest(String message) {
    return new ApiResponse<>(HttpStatus.BAD_REQUEST.value(), null, message, LocalDateTime.now());
  }
}
