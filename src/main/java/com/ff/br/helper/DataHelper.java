package com.ff.br.helper;

/** DataHelper */
public final class DataHelper {

  public static String getLastDayOfMonthUsingJodaTime(org.joda.time.LocalDate date) {
    return date.dayOfMonth().withMaximumValue().toString();
  }
}
