package com.ff.br.exception;

import com.ff.br.domain.dto.response.ApiResponse;
import java.net.UnknownHostException;
import java.time.LocalDateTime;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/** GlobalExceptionHandler */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

  @ExceptionHandler(BadCredentialsException.class)
  public ResponseEntity<ApiResponse<?>> badCredentialsException(BadCredentialsException e) {
    log.error("BadCredentialsException is thrown with message: {}", e.getMessage());
    return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
        .body(
            new ApiResponse<>(
                HttpStatus.UNAUTHORIZED.value(), null, e.getMessage(), LocalDateTime.now()));
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity<ApiResponse<?>> exception(Exception e) {
    log.error("Exception is thrown with message: {}", e.getMessage());
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
        .body(
            new ApiResponse<>(
                HttpStatus.INTERNAL_SERVER_ERROR.value(),
                null,
                e.getMessage(),
                LocalDateTime.now()));
  }

  @ExceptionHandler(UnknownHostException.class)
  public ResponseEntity<ApiResponse<?>> unknownHostException(UnknownHostException e) {
    log.error("UnknownHostException is thrown with message: {}", e.getMessage());
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
        .body(
            new ApiResponse<>(
                HttpStatus.INTERNAL_SERVER_ERROR.value(),
                null,
                e.getMessage(),
                LocalDateTime.now()));
  }
}
