package com.ff.br.configuration;

import com.ff.br.domain.entity.Employee;
import com.ff.br.domain.enums.Role;
import com.ff.br.repository.EmployeeRepository;
import com.github.javafaker.Faker;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;

@Slf4j
@RequiredArgsConstructor
public class DataGenerator {

  private final EmployeeRepository employeeRepository;
  private final PasswordEncoder passwordEncoder;

  public Employee insertEmployee() {
    Faker faker = new Faker();
    Employee employee =
        Employee.builder()
            .uuid(UUID.randomUUID().toString())
            .username(faker.name().username())
            .password(passwordEncoder.encode("123"))
            .role(faker.options().nextElement(List.of(Role.ROLE_ADMIN, Role.ROLE_USER)))
            .email(faker.internet().emailAddress())
            .numOfProject(faker.number().randomDigitNotZero())
            .build();
    return employeeRepository.save(employee);
  }

  public List<Employee> insertEmployeeData() {
    Employee employee =
        Employee.builder()
            .uuid(UUID.randomUUID().toString())
            .username("ndthinh")
            .fullName("Thinh Nguyen")
            .email("ndthinh4@cmcglobal.vn")
            .numOfProject(1)
            .password(passwordEncoder.encode("123"))
            .role(Role.ROLE_USER)
            .build();
    Employee employee1 =
        Employee.builder()
            .uuid(UUID.randomUUID().toString())
            .username("pmtuan")
            .fullName("Tuan Phung")
            .email("pmtuan4@cmcglobal.vn")
            .numOfProject(1)
            .password(passwordEncoder.encode("123"))
            .role(Role.ROLE_USER)
            .build();
    Employee employee2 =
        Employee.builder()
            .uuid(UUID.randomUUID().toString())
            .username("ndnminh")
            .fullName("Minh Nguyen")
            .email("ndnminh@cmcglobal.vn")
            .numOfProject(1)
            .password(passwordEncoder.encode("123"))
            .role(Role.ROLE_USER)
            .build();
    Employee employee3 =
        Employee.builder()
            .uuid(UUID.randomUUID().toString())
            .username("tttung")
            .fullName("Tung Trinh")
            .email("tttung7@cmcglobal.vn")
            .numOfProject(1)
            .password(passwordEncoder.encode("123"))
            .role(Role.ROLE_USER)
            .build();
    List<Employee> employees = new ArrayList<>();
    employees.add(employee);
    employees.add(employee1);
    employees.add(employee2);
    employees.add(employee3);
    return employeeRepository.saveAll(employees);
  }

  public void clean() {
    employeeRepository.deleteAll();
  }
}
