package com.ff.br.configuration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.testcontainers.service.connection.ServiceConnection;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.testcontainers.containers.PostgreSQLContainer;

@Slf4j
@TestConfiguration(proxyBeanMethods = false)
public class DatabaseTestConfig {
  private static PostgreSQLContainer<?> container;

  @Bean
  @ServiceConnection
  public PostgreSQLContainer<?> createPostgreSQLContainer(ConfigurableApplicationContext context) {
    if (container == null) {
      container = new PostgreSQLContainer<>("postgres:14.6");
      container
          .withUrlParam("serverTimezone", "GMT+7")
          .withDatabaseName("2ff")
          .withUsername("2ff")
          .withPassword("2ff")
          .start();
      var jdbcUrl = container.getJdbcUrl();

      context.addApplicationListener(
          applicationEvent -> {
            if (applicationEvent instanceof ApplicationReadyEvent) {
              log.info("Setting db url to '{}'", jdbcUrl);
            }
          });
    }
    return container;
  }
}
