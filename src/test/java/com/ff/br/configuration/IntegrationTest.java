package com.ff.br.configuration;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import org.junit.jupiter.api.Tag;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestConstructor;

@Retention(RetentionPolicy.RUNTIME)
@Tag("integration")
@ContextConfiguration(classes = {DatabaseTestConfig.class, DataGeneratorConfig.class})
@TestConstructor(autowireMode = TestConstructor.AutowireMode.ALL)
public @interface IntegrationTest {}
