package com.ff.br.configuration;

import com.ff.br.repository.EmployeeRepository;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;

@TestConfiguration
public class DataGeneratorConfig {

  @Bean
  public DataGenerator generator(
      EmployeeRepository employeeRepository, PasswordEncoder passwordEncoder) {
    return new DataGenerator(employeeRepository, passwordEncoder);
  }
}
