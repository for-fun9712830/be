package com.ff.br.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.ff.br.configuration.DataGenerator;
import com.ff.br.configuration.IntegrationTest;
import com.ff.br.domain.dto.request.SignInRequest;
import com.ff.br.domain.dto.response.SignInResponse;
import com.ff.br.domain.entity.Employee;
import com.ff.br.repository.EmployeeRepository;
import com.ff.br.service.impl.AuthenticationServiceImpl;
import com.ff.br.service.impl.JwtServiceImpl;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.test.util.ReflectionTestUtils;

@RequiredArgsConstructor
@SpringBootTest
@IntegrationTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AuthenticationServiceTest {

  private final DataGenerator generator;
  private AuthenticationService authenticationService;
  @Autowired private EmployeeRepository employeeRepository;
  @Autowired JwtService jwtService;
  @Autowired AuthenticationManager authenticationManager;
  private String username;

  @BeforeAll
  public void setup() {
    generator.clean();
    Employee savedEmployee = generator.insertEmployee();
    this.username = savedEmployee.getUsername();
    jwtService = new JwtServiceImpl();
    authenticationService =
        new AuthenticationServiceImpl(employeeRepository, jwtService, authenticationManager);
  }

  @AfterAll
  public void clean() {
    generator.clean();
  }

  @BeforeEach
  public void before() {
    ReflectionTestUtils.setField(
        jwtService, "accessTokenKey", "SE9NIE5BWSBUT0kgQlVPTiBNT1QgTUlOSCBUUkVOIFBITyBET05H");
    ReflectionTestUtils.setField(
        jwtService, "refreshTokenKey", "SE9NIE5BWSBUT0kgVlVJIE1PVCBNSU5IIFRSRU4gUEhPIERPTkc=");
  }

  @Test
  public void signIn_Valid() {
    SignInRequest request = new SignInRequest(username, "123");
    SignInResponse signInResponse = authenticationService.signIn(request);
    assertNotNull(signInResponse.getAccessToken());
    assertNotNull(signInResponse.getRefreshToken());
    assertNotNull(signInResponse.getType());
    assertNotNull(signInResponse.getExpiredIn());
    assertNotNull(signInResponse.getUsername());
  }
}
