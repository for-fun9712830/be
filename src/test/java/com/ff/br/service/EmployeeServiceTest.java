package com.ff.br.service;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.ff.br.configuration.DataGenerator;
import com.ff.br.configuration.IntegrationTest;
import com.ff.br.domain.entity.Employee;
import com.ff.br.repository.EmployeeRepository;
import com.ff.br.service.impl.EmployeeServiceImpl;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

@RequiredArgsConstructor
@SpringBootTest
@IntegrationTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class EmployeeServiceTest {

  private final DataGenerator generator;
  private EmployeeService employeeService;
  @Autowired private EmployeeRepository employeeRepository;
  @Autowired private final MailService mailService;

  private String username;

  @BeforeAll
  public void setup() {
    generator.clean();
    Employee savedEmployee = generator.insertEmployee();
    this.username = savedEmployee.getUsername();
    employeeService = new EmployeeServiceImpl(mailService, employeeRepository);
  }

  @AfterAll
  public void clean() {
    generator.clean();
  }

  @Test
  public void getEmployee_Valid() {
    Pageable pageable = PageRequest.of(0, 10);
    Page<Employee> employeePage = employeeService.getEmployee(pageable);
    assertNotNull(employeePage);
    assertFalse(employeePage.getContent().isEmpty());
  }
}
