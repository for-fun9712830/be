package com.ff.br.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.ff.br.configuration.DataGenerator;
import com.ff.br.configuration.IntegrationTest;
import com.ff.br.domain.entity.Employee;
import com.ff.br.repository.EmployeeRepository;
import com.ff.br.service.impl.JwtServiceImpl;
import com.ff.br.service.impl.UserServiceImpl;
import java.util.Map;
import java.util.Objects;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.util.ReflectionTestUtils;

@RequiredArgsConstructor
@SpringBootTest
@IntegrationTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class JwtServiceTest {

  private final DataGenerator generator;
  private JwtService jwtService;
  private UserService userService;
  @Autowired private EmployeeRepository employeeRepository;
  private String username;

  @BeforeAll
  public void setup() {
    generator.clean();
    Employee savedEmployee = generator.insertEmployee();
    this.username = savedEmployee.getUsername();
    jwtService = new JwtServiceImpl();
    userService = new UserServiceImpl(employeeRepository);
  }

  @AfterAll
  public void clean() {
    generator.clean();
  }

  @BeforeEach
  public void before() {
    ReflectionTestUtils.setField(
        jwtService, "accessTokenKey", "SE9NIE5BWSBUT0kgQlVPTiBNT1QgTUlOSCBUUkVOIFBITyBET05H");
    ReflectionTestUtils.setField(
        jwtService, "refreshTokenKey", "SE9NIE5BWSBUT0kgVlVJIE1PVCBNSU5IIFRSRU4gUEhPIERPTkc=");
  }

  @Test
  void extractUserName_Valid() {
    Map<String, Object> tokenMap = prepareData();
    assertNotNull(jwtService.extractUserName(String.valueOf(tokenMap.get("accessToken"))));
  }

  @Test
  void generateToken_Valid() {
    Map<String, Object> tokenMap = prepareData();
    assertTrue(Objects.nonNull(tokenMap.get("expiredIn")));
    assertTrue(Objects.nonNull(tokenMap.get("accessToken")));
    assertTrue(Objects.nonNull(tokenMap.get("refreshToken")));
  }

  @Test
  void isTokenValid_True() {
    UserDetails userDetails = userService.userDetailsService().loadUserByUsername(username);
    Map<String, Object> tokenMap = jwtService.generateToken(userDetails);
    assertTrue(jwtService.isTokenValid(String.valueOf(tokenMap.get("accessToken")), userDetails));
  }

  private Map<String, Object> prepareData() {
    UserDetails userDetails = userService.userDetailsService().loadUserByUsername(username);
    return jwtService.generateToken(userDetails);
  }
}
