package com.ff.br.service;

import static org.mockito.Mockito.mock;

import com.ff.br.configuration.DataGenerator;
import com.ff.br.configuration.IntegrationTest;
import com.ff.br.repository.EmployeeRepository;
import com.ff.br.service.impl.UserServiceImpl;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@RequiredArgsConstructor
@SpringBootTest
@IntegrationTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class UserServiceTest {

  private final DataGenerator generator;
  private UserService userService;

  @BeforeAll
  public void setup() {
    generator.clean();
    generator.insertEmployee();
    EmployeeRepository employeeRepository = mock(EmployeeRepository.class);
    userService = new UserServiceImpl(employeeRepository);
  }

  @AfterAll
  public void clean() {
    generator.clean();
  }

  @Test
  void userDetailsService_Valid() {
    Assertions.assertNotNull(userService.userDetailsService());
  }

  @Test
  void userDetailsService_Invalid() {
    String usernameNonExist = "test";
    Assertions.assertThrows(
        UsernameNotFoundException.class,
        () -> userService.userDetailsService().loadUserByUsername(usernameNonExist));
  }
}
