package com.ff.br;

import com.ff.br.configuration.DataGenerator;
import com.ff.br.configuration.DataGeneratorConfig;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestConstructor;

@SpringBootTest
@RequiredArgsConstructor
@Import(DataGeneratorConfig.class)
@TestConstructor(autowireMode = TestConstructor.AutowireMode.ALL)
class DataGeneratorTest {

  private static final int MAX_RECORD = 1;
  private final DataGenerator dataGenerator;

  @Test
  void insertDataTest() {
    dataGenerator.clean();
    for (int i = 0; i < MAX_RECORD; i++) {
      dataGenerator.insertEmployee();
    }
    dataGenerator.clean();
  }

  @Test
  void insertData() {
    dataGenerator.clean();
    dataGenerator.insertEmployeeData();
  }
}
