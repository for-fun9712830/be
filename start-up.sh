#!/bin/sh
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'
if ! command -v gradle &>/dev/null; then
  echo "${RED}Error: Gradle is not installed on your system. Please install Gradle before running this script, please follow up this below link: https://gradle.org/install/${NC}"
  exit 1
else
  echo "${GREEN}Gradle is installed.${NC}"
fi
gradle movePreCommitMessage
gradle bootRun -Dspring.profiles.active=local

