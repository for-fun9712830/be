FROM openjdk:17-alpine:3.14
ARG JAR_FILE=build/libs/*.jar
WORKDIR /app

RUN chown -R nobody:nobody /app
USER nobody
COPY --chown=nobody:nobody ${JAR_FILE} application.jar
EXPOSE 8081
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=dev","-Duser.timezone=GMT+7","-jar","application.jar"]